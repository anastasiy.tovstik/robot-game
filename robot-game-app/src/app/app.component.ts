import { Component, OnInit } from '@angular/core';

import { RobotStateService } from './services/robot-state.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  script: string;

  constructor(private robotStateService: RobotStateService) {
    this.script = 'POSITION 1 3 EAST\n'
      + 'FORWARD 3\n'
      + 'WAIT\n'
      + 'TURNAROUND\n'
      + 'FORWARD 1\n'
      + 'RIGHT\n'
      + 'FORWARD 2';
  }

  onSubmit() {
    this.script = this.script.toUpperCase();
    this.robotStateService.runScript(this.script);
  }

}

