import { RobotState } from 'src/app/classes/robot-state';

import { Component, Input, OnInit } from '@angular/core';

import { RobotStateService } from '../../services/robot-state.service';

@Component({
  selector: 'app-cell',
  templateUrl: './cell.component.html',
  styleUrls: ['./cell.component.css']
})
export class CellComponent implements OnInit {

  @Input()
  col: number;

  @Input()
  row: number;

  robot: RobotState;

  constructor(private robotStateService: RobotStateService) { }

  ngOnInit(): void {
    if (this.col === 0 && this.row === 0) {
      this.robot = new RobotState();
    }
    this.robotStateService.state.subscribe(state => {
      if (state.location.x === this.col && state.location.y === this.row) {
        this.robot = state;
      } else {
        this.robot = undefined;
      }
    });
  }
}
