import { Direction } from './direction';
import { Location } from './location';

export class RobotState {

    location: Location;
    direction: Direction;

    constructor() {
        this.location = new Location(0, 0);
        this.direction = Direction.EAST;
    }
}
