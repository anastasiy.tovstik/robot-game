import { Observable, Subject } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { environment } from '../../environments/environment';
import { RobotState } from '../classes/robot-state';

const ROBOT_STATE_SERVICE_URL = environment.serviceUrl + '/robot';

@Injectable()
export class RobotStateService {

    public state = new Subject<RobotState>();

    constructor(private http: HttpClient) { }

    public runScript(script: string): void {
        this.http.post<RobotState[]>(ROBOT_STATE_SERVICE_URL + '/commands/run_script', script)
            .pipe(catchError(this.handleError)).subscribe(response => {
                let i = 0;
                response.forEach(state => {
                    setTimeout(() => {
                        this.state.next(state);
                    }, 1500 * (i + 1));
                    i++;
                });
            });
    }

    private handleError(error: Response | any) {
        console.error('RobotStateService::handleError', error);
        return Observable.throw(error);
    }
}
