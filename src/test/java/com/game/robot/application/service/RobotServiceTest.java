package com.game.robot.application.service;

import com.game.robot.domain.grid.Direction;
import com.game.robot.domain.grid.Location;
import com.game.robot.domain.grid.RobotState;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;


@RunWith(SpringRunner.class)
public class RobotServiceTest {

    private RobotService robotService;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Before
    public void setUp() {
        this.robotService = new RobotServiceImpl();
    }

    @Test
    public void runScript_ValidScript_ReturnsRobotStatesList() {
        List<String> script = List.of(
                "POSITION 1 3 EAST",
                "FORWARD 3",
                "WAIT",
                "TURNAROUND",
                "FORWARD 1",
                "RIGHT",
                "FORWARD 2");
        List<RobotState> expectedRobotStates = List.of(
                new RobotState(new Location(1, 3), Direction.EAST),
                new RobotState(new Location(4, 3), Direction.EAST),
                new RobotState(new Location(4, 3), Direction.EAST),
                new RobotState(new Location(4, 3), Direction.WEST),
                new RobotState(new Location(3, 3), Direction.WEST),
                new RobotState(new Location(3, 3), Direction.NORTH),
                new RobotState(new Location(3, 1), Direction.NORTH)
        );
        List<RobotState> result = robotService.runScript(script);
        assertThat(result).isEqualTo(expectedRobotStates);
    }

    @Test
    public void runScript_EmptyScript_ReturnsEmptyRobotStatesList() {
        List<String> script = List.of();
        List<RobotState> result = robotService.runScript(script);
        assertThat(result).isEmpty();
    }

    @Test
    public void runScript_NoPositionCommand_ReturnsRobotStateWithDefaultPosition() {
        List<String> script = List.of("LEFT", "FORWARD 3");
        List<RobotState> expectedRobotStates = List.of(
                new RobotState(new Location(0, 0), Direction.SOUTH),
                new RobotState(new Location(0, 0), Direction.EAST),
                new RobotState(new Location(3, 0), Direction.EAST)
        );
        List<RobotState> result = robotService.runScript(script);
        assertThat(result).isEqualTo(expectedRobotStates);
    }

    @Test
    public void runScript_MoveForwardBeyondGrid_ReturnsRobotPositionAtBorder() {
        List<String> script = List.of("FORWARD 10");
        List<RobotState> expectedRobotStates = List.of(
                new RobotState(new Location(0, 0), Direction.SOUTH),
                new RobotState(new Location(0, 4), Direction.SOUTH)
        );
        List<RobotState> result = robotService.runScript(script);
        assertThat(result).isEqualTo(expectedRobotStates);
    }

    @Test
    public void runScript_TurnLeftRightCommands_ProduceCorrectResult() {
        String turnLeft = "LEFT";
        String turnRight = "RIGHT";
        List<String> script = List.of(turnLeft, turnLeft, turnLeft, turnLeft,
                turnRight, turnRight, turnRight, turnRight);
        RobotState expectedRobotState = new RobotState(new Location(0, 0), Direction.SOUTH);
        List<RobotState> result = robotService.runScript(script);
        assertThat(result).hasSize(9);
        assertThat(result.get(0)).isEqualTo(expectedRobotState);
        assertThat(result.get(result.size() - 1)).isEqualTo(expectedRobotState);
    }

    @Test
    public void runScript_InvalidCommand_ThrowsIllegalStateException() {
        expectedException.expect(IllegalStateException.class);
        expectedException.expectMessage("Unsupported action");
        List<String> script = List.of("POSITION 1 3 EAST", "JUMP");
        robotService.runScript(script);
    }

    @Test
    public void runScript_InvalidNumberOfCommandArguments_ThrowsIllegalArgumentException() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Command POSITION requires 3 parameters but got 2");
        List<String> script = List.of("POSITION 1 EAST");
        robotService.runScript(script);
    }

    @Test
    public void runScript_NullScript_ThrowsIllegalArgumentException() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Script must not be null");
        robotService.runScript(null);
    }

    @Test
    public void runScript_ScriptWithBlankLines_ReturnsRobotStatesList() {
        List<String> script = List.of(
                "POSITION 1 3 EAST",
                "FORWARD 3",
                "  ",
                "",
                "RIGHT",
                "FORWARD 1");
        List<RobotState> expectedRobotStates = List.of(
                new RobotState(new Location(1, 3), Direction.EAST),
                new RobotState(new Location(4, 3), Direction.EAST),
                new RobotState(new Location(4, 3), Direction.SOUTH),
                new RobotState(new Location(4, 4), Direction.SOUTH)
        );
        List<RobotState> result = robotService.runScript(script);
        assertThat(result).isEqualTo(expectedRobotStates);
    }

}