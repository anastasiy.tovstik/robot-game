package com.game.robot.application.service;

import com.game.robot.domain.grid.RobotState;

import java.util.List;

public interface RobotService {

    List<RobotState> runScript(List<String> script);
}
