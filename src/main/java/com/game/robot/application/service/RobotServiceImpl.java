package com.game.robot.application.service;

import com.game.robot.domain.action.*;
import com.game.robot.domain.grid.Direction;
import com.game.robot.domain.grid.GridContext;
import com.game.robot.domain.grid.Location;
import com.game.robot.domain.grid.RobotState;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

import static com.game.robot.domain.action.Command.*;

@Service
public class RobotServiceImpl implements RobotService {

    @Override
    public List<RobotState> runScript(List<String> script) {
        if (script == null) {
            throw new IllegalArgumentException("Script must not be null");
        }
        List<Action> actions = parseScript(script);
        GridContext context = new GridContext();
        for (Action action : actions) {
            action.execute(context);
        }
        return context.getStates();
    }

    private List<Action> parseScript(List<String> script) {
        return script.stream()
                .map(action -> action.split("\\b+ "))
                .filter(action -> !action[0].isBlank())
                .map(this::createAction)
                .collect(Collectors.toList());
    }

    private Action createAction(String... params) {
        switch (params[0]) {
            case POSITION:
                checkParams(params, 4);
                Location location = new Location(Integer.parseInt(params[1]),
                        Integer.parseInt(params[2]));
                Direction direction = Direction.valueOf(params[3]);
                return new PositionAction(location, direction);
            case WAIT:
                return new WaitAction();
            case FORWARD:
            case BACKWARD:
                checkParams(params, 2);
                return new MoveAction(params[0], Integer.parseInt(params[1]));
            case LEFT:
            case RIGHT:
            case TURNAROUND:
                return new TurnAction(params[0]);
            default:
                return new UnsupportedAction();
        }
    }

    private void checkParams(String[] params, int requiredNumber) {
        if (params.length < requiredNumber) {
            throw new IllegalArgumentException("Command " + params[0] +
                    " requires " + (requiredNumber - 1) + " parameters" +
                    " but got " + (params.length - 1));
        }
    }

}
