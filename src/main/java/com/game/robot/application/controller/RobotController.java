package com.game.robot.application.controller;

import com.game.robot.application.service.RobotService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/robot")
public class RobotController {

    private RobotService robotService;

    public RobotController(RobotService robotService) {
        this.robotService = robotService;
    }

    @PostMapping("/commands/run_script")
    public ResponseEntity runScript(@RequestBody(required = false) String script) {
        List<String> scriptTest = List.of(script.split("\\r?\\n"));
        return ResponseEntity.ok(robotService.runScript(scriptTest));
    }
}
