package com.game.robot.domain.action;

import com.game.robot.domain.grid.GridContext;
import com.game.robot.domain.grid.RobotState;

public class WaitAction implements Action {

    @Override
    public RobotState execute(GridContext gridContext) {
        RobotState currentState = gridContext.getCurrentRobotState();
        gridContext.setCurrentRobotState(currentState);
        return currentState;
    }
}
