package com.game.robot.domain.action;

public class Command {

    public static final String POSITION = "POSITION";
    public static final String FORWARD = "FORWARD";
    public static final String BACKWARD = "BACKWARD";
    public static final String WAIT = "WAIT";
    public static final String LEFT = "LEFT";
    public static final String RIGHT = "RIGHT";
    public static final String TURNAROUND = "TURNAROUND";

    private Command() {
        throw new IllegalStateException("Utility class");
    }

}
