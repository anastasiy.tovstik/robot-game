package com.game.robot.domain.action;

import com.game.robot.domain.grid.GridContext;
import com.game.robot.domain.grid.RobotState;

public class UnsupportedAction implements Action {

    @Override
    public RobotState execute(GridContext gridContext) {
        throw new IllegalStateException("Unsupported action");
    }
}
