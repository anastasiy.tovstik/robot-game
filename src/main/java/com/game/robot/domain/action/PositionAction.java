package com.game.robot.domain.action;

import com.game.robot.domain.grid.GridContext;
import com.game.robot.domain.grid.Direction;
import com.game.robot.domain.grid.Location;
import com.game.robot.domain.grid.RobotState;

public class PositionAction implements Action {

    private Location location;
    private Direction direction;

    public PositionAction(Location location, Direction direction) {
        this.location = location;
        this.direction = direction;
    }

    @Override
    public RobotState execute(GridContext gridContext) {
        RobotState robotState = new RobotState(location, direction);
        gridContext.setCurrentRobotState(robotState);
        return robotState;
    }
}
