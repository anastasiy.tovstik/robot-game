package com.game.robot.domain.action;

import com.game.robot.domain.grid.GridContext;
import com.game.robot.domain.grid.RobotState;

public interface Action {

    RobotState execute(GridContext gridContext);
}
