package com.game.robot.domain.action;

import com.game.robot.domain.grid.GridContext;
import com.game.robot.domain.grid.RobotState;

import static com.game.robot.domain.action.Command.BACKWARD;
import static com.game.robot.domain.action.Command.FORWARD;

public class MoveAction implements Action {

    private String moveType;
    private int steps;

    public MoveAction(String moveType, int steps) {
        this.moveType = moveType;
        this.steps = steps;
    }

    @Override
    public RobotState execute(GridContext gridContext) {
        switch (moveType) {
            case FORWARD:
                return moveForward(gridContext, steps);
            case BACKWARD:
                throw new IllegalStateException("Not implemented yet");
            default:
                return gridContext.getCurrentRobotState();
        }
    }

    private RobotState moveForward(GridContext gridContext, int steps) {
        RobotState currentState = gridContext.getCurrentRobotState();
        int x = currentState.getLocation().getX();
        int y = currentState.getLocation().getY();
        switch (currentState.getDirection()) {
            case NORTH:
                if (y - steps >= 0) {
                    currentState.getLocation().setY(y - steps);
                } else {
                    currentState.getLocation().setY(0);
                }
                break;
            case SOUTH:
                if (y + steps < gridContext.getSizeY()) {
                    currentState.getLocation().setY(y + steps);
                } else {
                    currentState.getLocation().setY(gridContext.getSizeY() - 1);
                }
                break;
            case WEST:
                if (x - steps >= 0) {
                    currentState.getLocation().setX(x - steps);
                } else {
                    currentState.getLocation().setX(0);
                }
                break;
            case EAST:
                if (x + steps < gridContext.getSizeX()) {
                    currentState.getLocation().setX(x + steps);
                } else {
                    currentState.getLocation().setX(gridContext.getSizeX() - 1);
                }
                break;
            default:
        }
        gridContext.setCurrentRobotState(currentState);
        return currentState;
    }
}
