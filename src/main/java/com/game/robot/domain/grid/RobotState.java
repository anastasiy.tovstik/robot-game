package com.game.robot.domain.grid;

import java.util.Objects;

public class RobotState {

    private Location location;
    private Direction direction;

    public RobotState() {
        this.location = new Location(0, 0);
        this.direction = Direction.SOUTH;
    }

    public RobotState(Location location, Direction direction) {
        this.location = location;
        this.direction = direction;
    }

    public RobotState(RobotState robotState) {
        this(new Location(robotState.getLocation()), robotState.getDirection());
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public Direction getDirection() {
        return direction;
    }

    public void setDirection(Direction direction) {
        this.direction = direction;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RobotState that = (RobotState) o;
        return Objects.equals(location, that.location) &&
                direction == that.direction;
    }

    @Override
    public int hashCode() {
        return Objects.hash(location, direction);
    }

    @Override
    public String toString() {
        return "RobotState{" +
                "location=" + location +
                ", direction=" + direction +
                '}';
    }
}
