package com.game.robot.domain.grid;

public enum Direction {

    NORTH, EAST, SOUTH, WEST

}
