package com.game.robot.domain.grid;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public final class GridContext {

    private final int sizeX;
    private final int sizeY;

    private List<RobotState> states;

    public GridContext(int sizeX, int sizeY) {
        this.states = new ArrayList<>();
        this.sizeX = sizeX;
        this.sizeY = sizeY;
    }

    public GridContext() {
        this(5, 5);
    }

    public int getSizeX() {
        return sizeX;
    }

    public int getSizeY() {
        return sizeY;
    }

    public RobotState getCurrentRobotState() {
        if (states.isEmpty()) {
            states.add(new RobotState());
        }
        return new RobotState(states.get(states.size() - 1));
    }

    public void setCurrentRobotState(RobotState currentRobotState) {
        this.states.add(new RobotState(currentRobotState));
    }

    public List<RobotState> getStates() {
        return Collections.unmodifiableList(states);
    }
}
